import React from 'react';
import {View} from 'react-native';
import Builder from "../../builder/";
import {GlobalLoaderConsumer, GlobalLoaderProvider} from "../../context/GlobalLoader";
import LoadingComponent from "../../common/LoadingComponent";

export default class Home extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            ContainerBuilder: null
        }
    }


    componentDidMount() {
        const {model} = this.props;
        const ContainerBuilder = Builder(model);

        this.setState({
            ContainerBuilder
        })
    }


    render() {
        const {ContainerBuilder} = this.state;

        return (
            <GlobalLoaderProvider style={{flex: 1}}>
                {ContainerBuilder && <ContainerBuilder componentId={this.props.componentId}/>}
                <GlobalLoaderConsumer>
                    {({loading}) => {

                        if (!loading) {
                            return (<View/>);
                        }

                        return (
                            <View
                                style={{
                                    width: "100%",
                                    height: "100%",
                                    position: "absolute",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                            >
                                <View
                                    style={{
                                        width: 50,
                                        height: 50,
                                        backgroundColor: "#000",
                                        borderRadius: 6
                                    }}>

                                    <LoadingComponent
                                        minHeight="auto"
                                        height="100%"
                                        alignItems="center"
                                        justifyContent="center"
                                    />
                                </View>
                            </View>
                        )
                    }}
                </GlobalLoaderConsumer>
            </GlobalLoaderProvider>
        )
    }
}

