import React from 'react';
import {View} from 'react-native';
import Builder from "../../builder/";

export default class Detail extends React.PureComponent {

    render() {
        const {data} = this.props;

        const ContainerBuilder = Builder(data, false);

        return (
            <View style={{flex: 1}}>
                <ContainerBuilder
                    backActive
                />
            </View>
        )
    }
}
