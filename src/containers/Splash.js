import React, {Component} from 'react';
import {View, Image, Alert, AsyncStorage} from "react-native";

import ApiResponder from "../api/ApiResponder";
import {goHome} from "../navigation";

class Splash extends Component {

    async componentDidMount() {
        try {
            const result = await ApiResponder.getALL();
            await AsyncStorage.setItem("data", JSON.stringify(result));
            goHome(result);
        } catch (err) {
            const result = await AsyncStorage.getItem("data");
            if (result) {
                goHome(result);
            } else {
                Alert.alert("Error", "Please check your internet for initial load data");
            }
        }
    }

    render() {
        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                <Image
                    source={require("../../assets/icons/logo.png")}
                />
            </View>
        );
    }
}

Splash.propTypes = {};

export default Splash;
