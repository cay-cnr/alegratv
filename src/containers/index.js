import {Navigation} from "react-native-navigation";
import Home from "./Home";
import Detail from "./Detail";
import Splash from "./Splash";


export default () => {
    Navigation.registerComponent('Splash', () => require('../containers/Splash').default);
    Navigation.registerComponent('Home', () => require('../containers/Home').default);
    Navigation.registerComponent('Detail', () => require('../containers/Detail').default);
}

