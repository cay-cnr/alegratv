import React from 'react';
import {View, Text} from "react-native";

import getStyle from "../../helper/getStyles";


export default class SectionComponent extends React.PureComponent {
    render() {
        const style = getStyle(this.props.data);
        return (
            <View style={{paddingLeft: 20}}>
                <Text style={style}>{this.props.data.text_c.text}</Text>
            </View>
        );
    }
};

SectionComponent.propTypes = {};
