import React from 'react';
import {FlatList} from "react-native";
import {mobileComponents} from "../../../config/Constant";
import getStyle from "../../helper/getStyles";

class HorizontalSliderComponent extends React.PureComponent {
    _renderItem = ({item: component}) => {
        const Component = mobileComponents[component.component_name];
        if (!Component) {
            return;
        }

        return (
            <Component
                data={component.data}
                componentId={this.props.componentId}
            />
        )
    };

    render() {
        const style = getStyle(this.props.data);

        return (
            <FlatList
                keyExtractor={(item) => `${item.component_hash}`}
                contentContainerStyle={[style, {height: "auto", justifyContent: "center", padding: 20}]}
                data={this.props.data.components}
                renderItem={this._renderItem}
                removeClippedSubviews={false}
                horizontal
                initialNumToRender={1}
                disableVirtualization/>
        );
    }
}

HorizontalSliderComponent.propTypes = {};

export default HorizontalSliderComponent;
