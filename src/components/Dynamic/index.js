import React from 'react';
import {View, Text, Dimensions, TouchableHighlight} from "react-native";
import FastImage from 'react-native-fast-image';

import getStyle from "../../helper/getStyles";
import getParameterByName from "../../helper/getParameterByName";
import ApiResponder from "../../api/ApiResponder";
import {GlobalLoaderConsumer} from "../../context/GlobalLoader";
import RNIap from 'react-native-iap';
import {Navigation} from "react-native-navigation";

const {width} = Dimensions.get("window");

const dynamicObjects = {
    image: {
        Component: FastImage,
        style: {
            resizeMode: "contain"
        },
    },
    text: {
        Component: Text,
    },
    view: {
        Component: TouchableHighlight
    }
};

const styles = {
    center: {
        alignItems: "center",
        justifyContent: "center"
    },
    left: {
        alignItems: "flex-start",
        justifyContent: "flex-start",
    }
};

class Dynamic extends React.PureComponent {

    constructor(props) {
        super(props);
        const {data: {dynamic_objects}, marginHorizontal} = this.props;

        this.state = {
            dynamic_objects: dynamic_objects || [],
            marginHorizontal,
            aspectRatio: 1,
            height: parseInt(this.props.data.height) || 200,
            width: 200,
            dynamicObject: null
        }
    }

    getText = (dynamicObject) => {
        const textStyle = getStyle(dynamicObject.text_detail);

        if (this.props.carousel) {

            return (
                <Text
                    key={dynamicObject.object_id}
                    style={textStyle}
                >
                    {dynamicObject.text_detail.text_c.text}
                </Text>
            )
        }
        const style = getStyle(dynamicObject);

        return (
            <View
                key={dynamicObject.object_id}
                style={([style, {
                    backgroundColor: "transparent",
                    alignItems: "center",
                    justifyContent: "center",
                    height: 50,
                    zIndex: 9999
                }])}>
                <Text
                    key={dynamicObject.object_id}
                    style={textStyle}
                >
                    {dynamicObject.text_detail.text_c.text}
                </Text>
            </View>
        )
    };

    getImage = (fitObject, index, style = {}, viewStyle = {}) => {
        if (Object.keys(style).length === 0) {
            style = getStyle(fitObject);
        }

        return (
            <View
                key={fitObject.object_id}
                style={[{
                    width: this.state.width,
                    height: this.state.height,
                    position: (style.width < 101 && style.height < 101) ? "absolute" : "relative",
                    ...viewStyle
                },
                    fitObject.position && fitObject.position.horizontal ? styles.center : styles.left,
                    this.props.carousel ? {flex: 1} : {},
                ]}>

                <FastImage
                    key={fitObject.object_id}
                    source={{uri: fitObject.image_detail.image}}
                    style={{
                        ...style,
                        backgroundColor: "transparent",
                        zIndex: index,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
            </View>
        )
    };

    fitObjects = (fitObjects, dynamicObject) => {
        if (fitObjects.length > 0) {
            return (
                <View
                    key={dynamicObject.object_id}
                    style={{
                        flexDirection: "row",
                        alignItems: "center",
                        height: "auto",
                        minHeight: 50,
                        width: "100%",
                        justifyContent: "center",
                    }}>
                    {
                        fitObjects.map((fitObject) => {
                            if (fitObject.type === "text") {

                                return this.getText(fitObject);

                            }
                            if (fitObject.type === "image") {
                                const style = getStyle(fitObject);

                                return (
                                    <FastImage
                                        key={fitObject.object_id}
                                        source={{uri: fitObject.image_detail.image}}
                                        style={{
                                            ...style,
                                            marginRight: 3,
                                            left: 0,
                                            right: 0
                                        }}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                )
                            }

                            return;
                        })
                    }
                </View>
            )
        }

        return null;
    };

    render() {
        const {dynamic_objects, marginHorizontal} = this.state;
        const {carousel} = this.props;

        if (dynamic_objects) {

            return (
                <GlobalLoaderConsumer>
                    {({loaderStatusChange}) => {
                        const Button = carousel ? View : TouchableHighlight;

                        return (
                            <Button
                                activeOpacity={1}
                                underlayColor={"#eee"}
                                onPress={async () => {
                                    const {dynamicObject} = this.state;

                                    if (!dynamicObject) {
                                        return;
                                    }

                                    const param = getParameterByName("video_gallery", dynamicObject.link_c.data.link);

                                    if (param) {
                                        loaderStatusChange(true);
                                        const data = await ApiResponder.getDataFromLink(`${dynamicObject.link_c.data.link}&response_type=json`);
                                        loaderStatusChange(false);

                                        if (data.page_url.includes("payment")) {
                                            try {
                                                await RNIap.consumeAllItems();
                                                await RNIap.buyProduct('android.test.purchased');
                                            } catch (err) {

                                            }
                                        } else {
                                            Navigation.push(this.props.componentId, {
                                                component: {
                                                    name: 'Detail',
                                                    passProps: {
                                                        data
                                                    }
                                                },
                                                options: {
                                                    topBar: {
                                                        visible: false,
                                                        height: 0
                                                    }
                                                },
                                            });
                                        }
                                    }
                                }}
                                style={{
                                    flex: 1,
                                    marginRight: carousel ? 0 : 20,
                                    alignItems: carousel ? "center" : "flex-start",
                                    marginTop: carousel ? -50 : 0,
                                    borderRadius: 10,
                                    zIndex: 0,
                                    justifyContent: carousel ? "center" : "flex-start",
                                }}>
                                <View>
                                    {dynamic_objects.map((dynamicObject, index) => {

                                        const Object = dynamicObjects[dynamicObject.type];
                                        if (!Object) {
                                            return;
                                        }
                                        if (dynamicObject.type === "text") {

                                            if (dynamicObject.size_to_fit) {
                                                return;
                                            }

                                            return this.getText(dynamicObject, index);
                                        }
                                        if (dynamicObject.type === "view") {
                                            if (dynamicObject.link_c) {
                                                this.setState({
                                                    dynamicObject
                                                });

                                                return;
                                            }
                                            return;
                                        }
                                        if (dynamicObject.type === "image") {
                                            const style = getStyle(dynamicObject);

                                            if (this.props.carousel) {
                                                const aspectRatio = dynamicObject.width / dynamicObject.height;
                                                style["width"] = width - marginHorizontal;
                                                style["height"] = (width - marginHorizontal) / aspectRatio;
                                            }

                                            if (dynamicObject.width > 101 && dynamicObject.height > 101) {
                                                this.setState({
                                                    width: parseInt(dynamicObject.width),
                                                    height: parseInt(dynamicObject.height)
                                                })
                                            }

                                            if (dynamicObject.image_detail.mode === "aspect_fit") {
                                                if (dynamicObject.related_position) {
                                                    return;
                                                }

                                                const fitObjects = dynamic_objects.filter((dynamicObject) => {
                                                    return (dynamicObject.image_detail && dynamicObject.image_detail.mode === "aspect_fit") || dynamicObject.size_to_fit
                                                });

                                                return this.fitObjects(fitObjects, dynamicObject);
                                            }

                                            return this.getImage(dynamicObject, index, style)

                                        }
                                        return null;
                                    })}
                                </View>
                            </Button>)
                    }}
                </GlobalLoaderConsumer>
            )
        }

        return (
            <View/>
        );
    }
}

Dynamic.propTypes = {};

export default Dynamic;
