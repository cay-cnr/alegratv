import React from 'react';
import {View, Text, Dimensions, StyleSheet} from "react-native";
import {mobileComponents} from "../../../config/Constant";
import Swiper from "react-native-swiper";

const {width} = Dimensions.get("window");

const styles = StyleSheet.create({
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    buttonText: {
        fontSize: 50,
        marginTop: -70,
        color: '#ff0000'
    },
    dotStyle: {
        marginBottom: -70
    }
});

class CarouselComponent extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            carouselList: [],
            loading: true
        };
    }


    _renderItem = ({item, marginHorizontal}) => {
        const Component = mobileComponents[item.component_name];

        if (Component) {
            return (
                <Component
                    key={item.component_hash}
                    data={item.data}
                    marginHorizontal={marginHorizontal}
                    carousel
                />
            );
        }

        return (
            <View/>
        )
    };

    render() {

        const {
            height,
            image_width,
            margin_between_views
        } = this.props.data;

        const aspectRatio = image_width / height;

        return (
            <View>
                <Swiper
                    dotStyle={styles.dotStyle}
                    activeDotStyle={styles.dotStyle}
                    autoplay
                    dotColor={"#ff0"}
                    activeDotColor={"#ff0000"}
                    width={width}
                    height={width / aspectRatio - 70}
                    prevButton={(<Text style={styles.buttonText}>‹</Text>)}
                    nextButton={(<Text style={styles.buttonText}>›</Text>)}
                    showsButtons={true}>
                    {
                        this.props.data.components.map((component, index) => {
                            return this._renderItem({
                                item: component,
                                index,
                                marginHorizontal: parseInt(margin_between_views)
                            });
                        })
                    }
                </Swiper>
            </View>
        );
    }
}

CarouselComponent.propTypes = {};

export default CarouselComponent;
