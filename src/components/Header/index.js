import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet, Dimensions} from 'react-native';

class Header extends React.PureComponent {
    constructor(props) {
        super(props);

        const header = this.props.data;

        this.style = StyleSheet.create({
            header: {
                width: Dimensions.get('window').width,
                height: 60,
                backgroundColor: header.background_colour,
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
            },

            leftLink: {
                flex: 1,
                padding: 15,
                alignItems: "flex-start"
            },

            rightLink: {
                flex: 1,
                padding: 15,
                alignItems: "flex-end"
            },
            backButtonStyle: {
                fontSize: 30,
            }
        })
    }

    render() {
        let leftLink = this.props.data.left_link_1 || {};
        let rightLink = this.props.data.right_link_1 || {};

        return (
            <View style={this.style.header}>
                <View style={this.style.leftLink}>
                    {this.props.backActive ? (
                        <TouchableOpacity
                            onPress={() => {
                                // navigation.
                            }}
                            style={{padding: 15}}>
                            <Text style={this.style.backButtonStyle}>‹</Text>
                        </TouchableOpacity>
                    ) : (
                        <Image
                            style={{width: 60, height: 60}}
                            source={{uri: leftLink.image}}
                        />
                    )}
                </View>

                <View style={{
                    alignItems: 'center',
                }}>
                    <Image
                        style={{
                            width: 150,
                            height: 50,
                        }}
                        source={{uri: this.props.data.title_image}}
                    />
                </View>

                <View style={this.style.rightLink}>
                    <Image
                        style={{width: 60, height: 60}}
                        source={{uri: rightLink.image}}
                    />
                </View>
            </View>
        )
    }
}

export default Header;
