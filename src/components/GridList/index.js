import React from 'react';
import {View} from "react-native";

import {mobileComponents} from "../../../config/Constant";

class GridList extends React.PureComponent {
    render() {

        return (
            <View style={{padding: 15}}>
                {this.props.data.components.map((component) => {
                    const Component = mobileComponents[component.component_name];

                    if (!Component) {
                        return;
                    }

                    return (
                        <Component
                            data={component.data}
                            gridList
                        />
                    )

                })}
            </View>
        );
    }
}

GridList.propTypes = {};

export default GridList;
