import React, {Component} from 'react';
import {TVEventHandler, View} from "react-native";
import VideoPlayer from 'react-native-af-video-player'

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center'
    }
};

class VideoComponent extends Component {

    componentDidMount() {
        this._tvEventHandler = new TVEventHandler();
        this._tvEventHandler.enable(this, (cmp, evt) => {
            if (evt && evt.eventType === 'select') {
                this.toggleVideo();
            }
        });
    }

    componentWillUnmount() {
        this._disableTVEventHandler();
    }

    _disableTVEventHandler() {
        if (this._tvEventHandler) {
            this._tvEventHandler.disable();
            delete this._tvEventHandler;
        }
    }

    toggleVideo = () => {
        this.videoPlayer.togglePlay();
    };


    render() {
        return (
            <View style={styles.container}>
                <VideoPlayer
                    ref={(videoPlayer) => {
                        this.videoPlayer = videoPlayer;
                    }}
                    url={this.props.data.video_source_url}
                    autoPlay={true}
                />
            </View>
        );
    }
}

VideoComponent.propTypes = {};

export default VideoComponent;
