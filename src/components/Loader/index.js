import React from 'react';
import {View} from "react-native";
import ApiResponder from "../../api/ApiResponder";
import LoadingComponent from "../../common/LoadingComponent";
import {mobileComponents} from "../../../config/Constant";

class Loader extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: []
        }
    }

    async componentWillMount() {
        const {link} = this.props.data;

        if (link) {
            try {
                const result = await ApiResponder.getDataFromLink(link);

                this.setState({
                    loading: false,
                    data: result
                })
            } catch (err) {
                console.log("err", err);
            }
        }
    }

    render() {

        if (this.state.loading || this.state.data.length === 0) {
            return (
                <LoadingComponent/>
            )
        }

        return (
            <View>
                {this.state.data.widgets.left_content.renders.map((render) => {
                    return render.components.map((component) => {
                        const Component = mobileComponents[component.component_name];

                        if (Component) {
                            return (
                                <Component
                                    key={component.component_hash}
                                    data={component.data}
                                    componentId={this.props.componentId}
                                />
                            )
                        }

                        return (
                            <View/>
                        )
                    });
                })}
            </View>
        );
    }
}

Loader.propTypes = {};

export default Loader;
