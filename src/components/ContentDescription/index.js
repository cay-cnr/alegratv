import React, {Component} from 'react';
import {View, Text} from "react-native";

import getStyle from "../../helper/getStyles";

class ContentDescription extends Component {
    render() {
        const {title_c, text_c, subtitle_c} = this.props.data;

        const titleStyle = getStyle(title_c);
        const textStyle = getStyle(text_c);
        const subtitleStyle = getStyle(subtitle_c);

        return (
            <View style={[{padding: 30}]}>
                <Text style={titleStyle}>{title_c.text}</Text>
                <Text style={subtitleStyle}>{subtitle_c.text}</Text>
                <Text style={textStyle}>{text_c.text}</Text>
            </View>
        );
    }
}

ContentDescription.propTypes = {};

export default ContentDescription;
