import React from 'react';
import {View} from "react-native";

import {mobileModules} from "../../../config/Constant";


class HeaderTop extends React.PureComponent {
    render() {

        return (
            <View>
                {this.props.renders.map((render) => {
                    const Module = mobileModules[render.module_name];

                    if (!Module) {
                        return;
                    }

                    return (
                        <Module
                            componentId={this.props.componentId}
                            key={render.module_id}
                            render={render}
                        />
                    )
                })}
            </View>
        );
    }
}

HeaderTop.propTypes = {};

export default HeaderTop;
