import React from 'react';
import {View} from "react-native";
import {mobileModules} from "../../../config/Constant";


export default ({renders, componentId}) => {
    return (
        <View>
            {renders.map((render) => {
                const Module = mobileModules[render.module_name];

                if (!Module) {
                    return;
                }

                return (
                    <Module
                        key={render.module_name}
                        render={render}
                        componentId={componentId}
                    />
                )
            })}
        </View>
    )
}
