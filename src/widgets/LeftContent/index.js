import React from 'react';
import {View, FlatList, InteractionManager} from "react-native";

import {mobileModules} from "../../../config/Constant";

class LeftContent extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            renders: []
        }
    }


    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                renders: this.props.renders.filter((render, index) => index > 5 || index < 2)
            })
        })
    }


    renderItem = ({item: render}) => {
        const Module = mobileModules[render.module_name];

        if (!Module) {
            return;
        }

        return (
            <Module
                render={render}
                componentId={this.props.componentId}
            />
        )
    };

    render() {
        return (
            <View>
                <FlatList
                    keyExtractor={(item) => `${item.module_id}`}
                    data={this.state.renders}
                    renderItem={this.renderItem}
                    numColumns={1}
                    updateCellsBatchingPeriod={10}
                    disableVirtualization
                    initialNumToRender={1}
                    removeClippedSubviews
                    maxToRenderPerBatch={1}
                    keyboardShouldPersistTaps={'handled'}
                />
            </View>
        );
    }
}

LeftContent.propTypes = {};

export default LeftContent;
