import React from "react";
import {View, ActivityIndicator} from "react-native";

export default (style) => {
    return (
        <View style={{minHeight: 200, width: "100%", alignItems: "center", justifyContent: "center", ...style}}>
            <ActivityIndicator size="large"/>
        </View>
    )
}
