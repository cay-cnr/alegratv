import React from "react";

const GlobalLoaderContext = React.createContext();

export class GlobalLoaderProvider extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        };
    }

    loaderStatusChange = (loading) => {
        this.setState({
            isLoading: loading,
        });
    };

    render() {
        const {children} = this.props;

        return (
            <GlobalLoaderContext.Provider
                value={{
                    loaderStatusChange: this.loaderStatusChange,
                    loading: this.state.isLoading,
                }}
            >
                {children}
            </GlobalLoaderContext.Provider>
        );
    }
}

export const GlobalLoaderConsumer = GlobalLoaderContext.Consumer;
