import React from 'react';
import {FlatList} from "react-native";
import {mobileWidgets} from "../../config/Constant";

export default (data, headerActive = true) => {
    if (!data.widgets) {
        return;
    }

    return class Builder extends React.PureComponent {
        static defaultProps = {
            componentId: ""
        };

        _renderItem = ({item: widget}) => {

            const Widget = mobileWidgets[widget];

            if (!Widget || (!headerActive && widget === "header")) {
                return;
            }
            return (
                <Widget
                    renders={data.widgets[widget].renders}
                    componentId={this.props.componentId}
                />
            )
        };

        render() {
            return (
                <FlatList
                    keyExtractor={(item) => item}
                    data={Object.keys(data.widgets)}
                    removeClippedSubviews
                    keyboardShouldPersistTaps={'handled'}
                    renderItem={this._renderItem}/>
            )
        }
    }
};
