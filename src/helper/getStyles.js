// import hex2rgba from './hex2rgba';
// import evil from './evil';


import {STYLE_MAP, SUB_STYLES} from "../../config/Constant";

export const getStyleJSON = (data = {}, bypassStyles = [], bypassSubstyles = [], style = {}) => {
    Object.keys(data).forEach((key) => {
        let value = data[key];

        if (bypassStyles.indexOf(key) !== -1 || bypassSubstyles.indexOf(key) !== -1 || ['class_name', 'subname'].indexOf(key) !== -1) return;

        if (SUB_STYLES.indexOf(key) !== -1) {
            style = Object.assign({}, style, getStyleJSON(data[key], bypassStyles, bypassSubstyles, {}));
        } else if (Object.hasOwnProperty.call(STYLE_MAP, key)) {

            switch (key) {
                case 'strike_through':
                    if (value !== 0 && data.is_strikethrough !== false) {
                        style['textDecoration'] = 'line-through';
                    }
                    break;

                case 'corner_radius': {
                    value = typeof value === "string" ? parseInt(value) : value || 0;

                    const rightBottom = data.right_bottom_corner && data.right_bottom_corner === true;
                    const rightTop = data.right_top_corner && data.right_top_corner === true;
                    const leftTop = data.left_top_corner && data.left_top_corner === true;
                    const leftBottom = data.left_bottom_corner && data.left_bottom_corner === true;

                    if (rightBottom || rightTop || leftBottom || leftTop) {
                        if (leftTop === true) {
                            style['borderTopLeftRadius'] = value;
                        }
                        if (rightTop === true) {
                            style['borderTopRightRadius'] = value;
                        }
                        if (leftBottom === true) {
                            style['borderBottomLeftRadius'] = value;
                        }
                        if (rightBottom === true) {
                            style['borderBottomRightRadius'] = value;
                        }
                    } else {
                        try {
                            style['borderRadius'] = parseFloat(value);
                        } catch (err) {
                            console.log("err", err);

                        }
                    }
                    break;
                }
                case 'height':
                    if (typeof value === 'number') {
                        style.height = value;
                    } else {
                        try {
                            style.height = parseInt(value);
                        } catch (err) {
                            console.log("err", err);
                        }
                    }

                    break;

                case 'width':
                    if (typeof value === 'number') {
                        style.width = value;
                    } else {
                        try {
                            style.width = parseInt(value);
                        } catch (err) {
                            console.log("err", err);
                        }
                    }

                    break;

                case 'background_colour':
                    style['backgroundColor'] = value;
                    break;

                case 'is_hidden':
                    style.display = value === true ? 'none' : 'block';
                    break;
                case "text_size":
                    style.fontSize = parseInt(value);
                    break;
                case "position":

                    if (value.horizontal) {
                        style.marginHorizontal = parseInt(value.horizontal);
                    }
                    if (value.vertical) {
                        style.marginVertical = parseInt(value.vertical);
                    }
                    if (value.left) {
                        style.left = parseInt(value.left);
                    }
                    if (value.top) {
                        style.top = parseInt(value.top);
                    }

                    break;
                default:
                    if (value !== null) {
                        if (key === 'font') {
                            value = `${value}, sans-serif`;
                        }

                        // else {
                        //     value = evil(value);
                        //
                        //     if (!isNaN(value)) {
                        //         value += 'px';
                        //     }
                        //
                        //     if (value.indexOf('%') === 0) {
                        //         value = `${value.substr(1, value.length)}%`;
                        //     }
                        // }

                        style[(STYLE_MAP[key] === null ? key : STYLE_MAP[key])] = value;
                    }
                    break;
            }
        }
    });

    if (Object.hasOwnProperty.call(style, 'borderColor')) {

        if (style['borderColor'].replace('px', '') !== '') {
            if (style['borderLeftWidth'] || style['borderRightWidth'] || style['borderTopWidth'] || style['borderBottomWidth']) {
                style['borderStyle'] = 'solid';
            } else {
                style['borderStyle'] = 'solid';

                if (data.border_position) {
                    const borderKey = (data.border_position === 'center' ? 'Bottom' : data.border_position);

                    style['borderWidth'] = 0;
                    style[`border${capitalizeFirstLetter(borderKey)}Width`] = 1;
                } else {
                    style['borderWidth'] = 1;
                }
            }

            if (style['borderLeftWidth'] === 0
                && style['borderRightWidth'] === 0
                && style['borderTopWidth'] === 0
                && style['borderBottomWidth'] === 0
            ) {
                style['borderBottomWidth'] = 1;
            }
        }
    }

    return style;
};

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const getStyle = (data, bypassStyles = [], bypassSubstyles = [], style = {}) => {
    const styleJSON = getStyleJSON(data, bypassStyles, bypassSubstyles, style);
    const styleJSONKeys = Object.keys(styleJSON);

    if (!styleJSONKeys.length) return {};


    return styleJSON;
};

export default getStyle;
