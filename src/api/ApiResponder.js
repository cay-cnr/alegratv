import {DEFAULT_API_URL, DEFAULT_TIME_OUT} from "../../config/Constant";

const waitFor = (timeout = DEFAULT_TIME_OUT) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, timeout)
    })
};


export default {
    getALL: async () => {
        // just for kidding

        return fetch(`${DEFAULT_API_URL}?response_type=json`).then((response) => {
            return response.json();
        });
    },
    getDataFromLink: async (link) => {
        return fetch(DEFAULT_API_URL + link).then((response) => {
            return response.json();
        })
    }
}
