import {Navigation} from "react-native-navigation";

export const goHome = (model) => Navigation.setRoot({
    root: {
        stack: {
            id: 'App',
            options: {
                topBar: {
                    visible: false,
                    height: 0
                }
            },
            children: [
                {
                    component: {
                        name: 'Home',
                        passProps: {
                            model
                        }
                    }
                }
            ],
        }
    }
})
