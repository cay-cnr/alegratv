import React from 'react';
import  {View} from "react-native";
import PropTypes from 'prop-types';
import {mobileComponents} from "../../../config/Constant";

class MobileInlinePlayer extends React.PureComponent {
    render() {
        return (
            <View>
                {this.props.render.components.map((component) => {
                    const Component = mobileComponents[component.component_name];

                    if (!Component) {
                        return;
                    }

                    return (
                        <Component
                            key={component.component_hash}
                            data={component.data}
                            backActive={this.props.backActive}
                            componentId={this.props.componentId}

                        />
                    )
                })}
            </View>
        );
    }
}

MobileInlinePlayer.propTypes = {};

export default MobileInlinePlayer;
