import React from 'react';
import {View, InteractionManager} from "react-native";

import {mobileComponents} from "../../../config/Constant";

class HorizontalList extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            components: []
        }
    }


    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                components: this.props.render.components,
            })
        });
    }


    render() {
        return (
            <View>
                {this.state.components.length > 0 && this.state.components.map((component) => {
                    const Component = mobileComponents[component.component_name];

                    if (!Component) {
                        return;
                    }

                    return (
                        <Component
                            key={component.component_hash}
                            data={component.data}
                            componentId={this.props.componentId}
                        />
                    )
                })}
            </View>
        );
    }
}

HorizontalList.propTypes = {};

export default HorizontalList;
