import React, {Component} from 'react';
import {View} from "react-native";

import {mobileComponents} from "../../../config/Constant";

class MobileHeader extends Component {
    render() {
        return (
            <View>
                {this.props.render.components.map((component) => {

                    const Component = mobileComponents[component.component_name];

                    if (!Component) {
                        return;
                    }

                    return (
                        <Component
                            key={component.component_hash}
                            data={component.data}
                            backActive={this.props.backActive}
                            componentId={this.props.componentId}

                        />
                    )
                })}
            </View>
        );
    }
}

MobileHeader.propTypes = {};

export default MobileHeader;
