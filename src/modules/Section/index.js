import React, {Component} from 'react';
import {View} from "react-native";

import {mobileComponents} from "../../../config/Constant";

class Section extends Component {
    render() {
        return (
            <View style={{marginTop:30}}>
                {this.props.render.components.map((component) => {
                    const Component = mobileComponents[component.component_name];

                    if (!Component) {
                        return;
                    }

                    return (
                        <Component
                            key={component.component_hash}
                            data={component.data}
                            componentId={this.props.componentId}

                        />
                    );
                })}
            </View>
        );
    }
}

Section.propTypes = {};

export default Section;
