import React, {Component} from 'react';
import {ScrollView} from "react-native";

import {mobileComponents} from "../../../config/Constant";

class MobileGridList extends Component {
    render() {

        return (
            <ScrollView
                horizontal
            >
                {this.props.render.components.map((component) => {
                    const Component = mobileComponents[component.component_name];

                    if (!Component) {
                        return;
                    }

                    return (
                        <Component
                            key={component.component_hash}
                            data={component.data}
                            backActive={this.props.backActive}
                            componentId={this.props.componentId}

                        />
                    )
                })}
            </ScrollView>
        );
    }
}

MobileGridList.propTypes = {};

export default MobileGridList;
