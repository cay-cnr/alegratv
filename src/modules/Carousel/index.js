import React from 'react';
import {View, Text} from "react-native";
import {mobileComponents} from "../../../config/Constant";

class Carousel extends React.PureComponent  {

    constructor(props) {
        super(props);
        this.state = {
            carouselList: [],
            loading: true
        };
    }

    render() {
        return (
            <View>
                {
                    this.props.render.components.map((component) => {
                        const Component = mobileComponents[component.component_name];

                        if (!Component) {
                            return;
                        }

                        return (
                            <Component
                                key={component.component_hash}
                                data={component.data}
                                componentId={this.props.componentId}
                            />
                        );
                    })
                }

            </View>
        );
    }
}

Carousel.propTypes = {};

export default Carousel;
