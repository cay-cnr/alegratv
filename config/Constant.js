import Carousel from "../src/modules/Carousel";
import HorizontalList from "../src/modules/HorizontalList";
import Section from "../src/modules/Section";
import Loader from "../src/components/Loader";
import Dynamic from "../src/components/Dynamic";
import CarouselComponent from "../src/components/CarouselComponent";
import SectionComponent from "../src/components/Section";
import HorizontalSliderComponent from "../src/components/HorizontalSliderComponent";
import VideoComponent from "../src/components/VideoComponent";
import HeaderTop from "../src/widgets/HeaderTop";
import LeftContent from "../src/widgets/LeftContent";
import HeaderWidget from "../src/widgets/Header";
import Header from "../src/components/Header";
import MobileHeader from "../src/modules/MobileHeader";
import MobileInlinePlayer from "../src/modules/MobileInlinePlayer";
import MobileGridList from "../src/modules/MobileGridList";
import MobileContentDescription from "../src/modules/MobileContentDescription";
import ContentDescription from "../src/components/ContentDescription";

export const DEFAULT_TIME_OUT = 300;
export const DEFAULT_API_URL = "http://davinci.test.alegra.io";

export const mobileModules = {
    "mobile_carousel": Carousel,
    "mobile_horizontal_list": HorizontalList,
    "ca_mobile_section": Section,
    "ca_mobile_header": MobileHeader,
    "mobile_inline_video_player": MobileInlinePlayer,
    "mobile_grid_list_style3": MobileGridList,
    "mobile_content_description": MobileContentDescription
};

export const mobileComponents = {
    "LoadMore": Loader,
    "Dynamic": Dynamic,
    "Carousel": CarouselComponent,
    "Section": SectionComponent,
    "HorizontalSlider": HorizontalSliderComponent,
    "Video": VideoComponent,
    "Header": Header,
    "ContentDescription": ContentDescription
};

export const mobileWidgets = {
    "header_top": HeaderTop,
    "left_content": LeftContent,
    "header": HeaderWidget,

};

export const STYLE_MAP = {
    background_colour: 'backgroundColor',
    text_colour: 'color',
    border_colour: 'borderColor',
    text_align: 'textAlign',
    text_position: 'textAlign',
    position: "position",
    top_margin: 'marginTop',
    bottom_margin: 'marginBottom',
    left_margin: 'marginLeft',
    right_margin: 'marginRight',
    corner_radius: 'borderRadius',
    top_border: 'borderTopWidth',
    bottom_border: 'borderBottomWidth',
    left_border: 'borderLeftWidth',
    right_border: 'borderRightWidth',
    text_size: 'fontSize',
    font: 'fontFamily',
    top_padding: 'paddingTop',
    left_padding: 'paddingLeft',
    right_padding: 'paddingRight',
    bottom_padding: 'paddingBottom',
    // x: 'left',
    // y: 'top',
    // top: null,
    // left: null,
    // bottom: null,
    // right: null,
    // image: null,
    height: null,
    color: null,
    strike_through: 'textDecoration',
    width: null,
    is_hidden: null,
    min_height: 'minHeight',
    padding_left: 'paddingLeft',
    padding_right: 'paddingRight',
    padding_top: 'paddingTop',
    padding_bottom: 'paddingBottom',

};
export const PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhoPuwXNykol7IJzZn2WmitA6siTZ8qXBgW+/rE70/NxouSDbxsSB4T\n" +
    "/7VJGaCt24An9NP0R44X+xchPSH93RSrYjBwbQIBAVa8CoL0iJQTjvNRCOAMCvIYtzasvwi9X22YKqppWwC6dVncdhjZz2Z67+a9HGdCHrw3poMVfeVLqO\n" +
    "+cjDU7dIaJgFR2tqwjP26WevUOQq+y4eLwtxgKyRaFs4MP7vXopKpKLBVtv2taHEIoh2032103gu/kLxPU+Kwu5q1cMazygg\n" +
    "/dXRpqNwIiPtCZ5mty8pydrPH5cxINXRZURvt0CvqAnEBqhwO4JsChLLgPUgglzCwsehY8W1NwIDAQAB";
export const SUB_STYLES = ['title_c', 'text_c'];

